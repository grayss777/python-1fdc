FROM  tiangolo/uwsgi-nginx-flask:python3.9
MAINTAINER Sergey Stekolnikov 'grayss777@yandex.ru'
RUN apt-get install wget
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz
COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY . /app
WORKDIR /app
CMD ["dockerize", "-wait",  "tcp://postgres:5432", "python", "main.py"]
CMD ["uwsgi", "test_apps.ini"]

