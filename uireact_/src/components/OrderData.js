function OrderData(props) {

    const { order } = props

    if (!order || order.length === 0) return <p>Нет данных.</p>

    return (

        <div>

            <table>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Заказ №</th>
                        <th>цены $</th>
                        <th>цены Rub</th>
                        <th>Дата поставки</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        order.map((order) =>
                                <tr key={order.n}>
                                <td>{order.n}</td>
                                <td>{order.order}</td>
                                <td>{order.cost}</td>
                                <td>{order.cost_r}</td>
                                <td>{order.term}</td>

                            </tr>
                        )
                    }
                </tbody>
            </table>
      </div>
    )
}

export default OrderData