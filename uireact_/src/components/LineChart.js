import React, { useState, useEffect } from 'react'
import axios from 'axios';

import OrderData from './OrderData'
import LoadingOrderData from './LoadingOrderData'





import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';

import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);



const LineChart = () => {
  const [chart, setAppState] = useState({})

    const DataLoading =  LoadingOrderData(OrderData);



useEffect(function() {
    setAppState({loading: true})

    const apiUrl = '/get_api';

    axios.get(apiUrl,{ crossDomain: false , headers:{'Access-Control-Allow-Origin': '*'}}).then((resp) => {
      const allOrder = resp.data;

      setAppState({
      loading: false,
      order: allOrder

       });
    });
  }, [setAppState]);

  console.log("chart", chart);
  var data = {
    labels: chart?.order?.map(x => x.term),
    datasets: [{
      label: `${chart?.order?.length} заказов`,
      data: chart?.order?.map(x => x.cost),
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      animation: false,
      borderWidth: 1
    }]
  };

  var options = {
    maintainAspectRatio: false,
    scales: {
    },
    legend: {
      labels: {
        fontSize: 25,
      },
    },
  }

  return (
  <div>
    <div className="my_chart">
      <Line
        data={data}
        height={400}
        options={options}

      />

    </div>
<div>      <DataLoading isLoading={chart.loading}  order={chart.order} /></div>
    </div>
  )
}

class Line_up extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
    LineChart_const : <LineChart/>
    };

    }

componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {   clearInterval(this.timerID); }

  tick() {    this.setState({
                            LineChart_const : <LineChart/> });


                            }

  render() {return (
<div>
 {this.state.LineChart_const}
</div>
)
}
    }

export default Line_up