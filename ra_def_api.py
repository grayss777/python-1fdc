# -*- coding: utf8 -*-
from BD_con import *
from ra_apps_bd import *
from ra_my_service import *
from pprint import pprint
import requests
from lxml import etree
from datetime import datetime

log = Ra_log('ra_def_api')
log.ra_info('Hi! Start ra_def_api')


def format_cost(s):  # рекурсивная функция
    if len(s) <= 3:
        return s
    else:
        print('-s=', s)
        return format_cost(s[0:-3]) + " " + s[-3:len(s)]


def format_to_rub(sum):  # Форматирует число в читаемы й вид 50000 -> 50 000 р.
    sum_ = str(sum).split('.')
    if len(sum_) == 2:
        sum_r = sum_[0]
        sum_k = sum_[1]
    else:
        sum_r = sum
        sum_k = ''

    result = format_cost(str(sum_r))
    return result + '.' + sum_k + 'р.'


class sheet_item():
    def __init__(self, col_name_by_bd):
        """Constructor"""
        self.n = col_name_by_bd['n']
        self.order = col_name_by_bd['order']
        self.cost = col_name_by_bd['cost']
        self.cost_r = col_name_by_bd['cost_r']
        self.term = col_name_by_bd['term']
        self.isset = True

    def test_col_name_by_db(self, col_name_by_bd):
        if col_name_by_bd.get('cost') and col_name_by_bd.get('cost_r') and col_name_by_bd.get('term') and \
                col_name_by_bd.get('n') and col_name_by_bd.get('order'):
            return True
        else:
            return False

    def get_diff(self, col_name_by_bd):  # col_name_by_bd - словарь из class Ra_sheets():
        self.isset = True
        diff = False
        if self.test_col_name_by_db(col_name_by_bd):
            if float(col_name_by_bd['cost']) != float(self.cost):
                self.cost = col_name_by_bd['cost']
                diff = True
                print('=diff cost=', diff)
            if float(col_name_by_bd['cost']) != float(self.cost):
                self.cost_r = col_name_by_bd['cost_r']
                diff = True
                print('=diff cost=', diff)
            if col_name_by_bd['term'] != self.term:
                self.term = col_name_by_bd['term']
                diff = True
                print('=diff term=', diff)
            if diff:
                self.n = col_name_by_bd['n']
                self.order = col_name_by_bd['order']
            if diff:
                print('Update ', diff)
            if diff:
                self.update_db()  ## DB
            return diff
        else:
            log.ra_warning('Err get_diff')
            return False

    def update_db(self):
        db_item = Test_sheets(n=self.n, n_order=self.order, cost=self.cost, cost_r=self.cost_r, term=self.term)
        db_item.update(self.order)
        return db_item.update(self.order)

    def add_to_bd(self):  # функция добавляет заказ в # DB
        new_test = Test_sheets(n=self.n, n_order=self.order,
                               cost=self.cost, cost_r=self.cost_r,
                               term=self.term)
        new_test.add()


class Ra_sheets():
    def __init__(self):
        """Constructor"""
        self.URL_cbr = 'https://www.cbr.ru/scripts/XML_daily.asp?date_req={}'
        self.key = {}  # ключи
        self.key['n'] = 'n'
        self.key['order'] = 'order'
        self.key['cost'] = 'cost'
        self.key['cost_r'] = 'cost_r'
        self.key['term'] = 'term'
        self.key['prefix'] = 'order_'
        self.n_in_row = {}
        self.exchange_rate = '0'  # Стоимость одного доллара
        self.last_date_updae_exchange_rate = ''
        self.get_exchange_rate()
        self.col_name = {}  # имена колонок таблицы
        self.col_name[self.key['n']] = '№'
        self.col_name[self.key['order']] = 'заказ №'
        self.col_name[self.key['cost']] = 'стоимость,$'
        self.col_name[self.key['cost_r']] = 'стоимость, р'
        self.col_name[self.key['term']] = 'срок поставки'
        self.col_name_by_bd = {}  # значения из таблицы
        self.col_name_by_bd[self.key['n']] = ''
        self.col_name_by_bd[self.key['order']] = ''
        self.col_name_by_bd[self.key['cost']] = ''
        self.col_name_by_bd[self.key['cost_r']] = ''
        self.col_name_by_bd[self.key['term']] = ''
        self.len_column = -1  # колличество строк
        self.sheets_order_dic = {}  # словарь с заказами

    def scan_reset_isset_sheet_item(self):
        for order in self.sheets_order_dic:
            self.sheets_order_dic[order].isset = False

    def delete_orders_self(self, arr):
        not_err = True
        if len(arr) >= 1:
            for it in arr:
                if self.sheets_order_dic.get(self.key['prefix'] + str(it)):
                    del self.sheets_order_dic[self.key['prefix'] + str(it)]
                else:
                    not_err = False
                    log.ra_warning(' err delete order ' + str(it))
            delete_order(arr)  # Удаление из БД
        return not_err

    def scan_isset_sheet_item(self):  # возвращает массив с номерами заказов для удаления
        order_for_del_arr = [self.sheets_order_dic[order].order for order in self.sheets_order_dic if
                             self.sheets_order_dic[order].isset == False]
        return order_for_del_arr

    def update_len_column(self, val):
        self.len_column = val

    def get_len_column(self):
        return self.len_column

    def date_update_exchange_rate(self, val):  # если дата меняется, то она перезаписывается
        if self.last_date_updae_exchange_rate != val:
            self.last_date_updae_exchange_rate = val
            log.ra_info('Стоит обновть обменный курс!')
            return True
        else:
            return False

    def update_exchange_rate(self, new_exchange_rate):
        try:

            float_exch_r = float(new_exchange_rate.replace(',', '.'))
            self.exchange_rate = float_exch_r
            return True
        except Exception as e:
            log.ra_warning('Обменный курс не обновлен! Не верное значение!')
            return False

    def get_exchange_rate(self):

        date_nuw = datetime.now()
        nuw_date = date_nuw.strftime("%d/%m/%Y")
        if self.date_update_exchange_rate(val=nuw_date):
            log.ra_info('Запрашиваю обменный курс!')
            base_url = self.URL_cbr.format(nuw_date)
            id_course = 'R01235'
            responce = requests.get(base_url)
            if responce.status_code != 200:
                raise ValueError(f"Request add_user failed {responce.status_code}")
            resp_xml_content = responce.content
            tree = etree.XML(resp_xml_content)
            result = tree.find("Valute[@ID='{}']".format(id_course))
            result2 = result.find('Value')
            log.ra_info('обменный курс {} за доллор'.format(result2.text))
            self.update_exchange_rate(result2.text)

    def start(self):
        array_sheet_item = self.get_sheets()

        for name_column in self.col_name:  # сопостовляем параметры заказа с колонкой  в таблице
            i = 0
            for name_column_tab in array_sheet_item[0]:
                if self.col_name[name_column] == name_column_tab:
                    self.n_in_row[name_column] = i
                i += 1
        del array_sheet_item[0]         # удаляем названия колонок из масива
        for row in array_sheet_item:
            print(row[self.n_in_row[self.key['order']]])
            self.col_name_by_bd[self.key['order']] = row[self.n_in_row[self.key['order']]]
            self.col_name_by_bd[self.key['n']] = row[self.n_in_row[self.key['n']]]
            self.col_name_by_bd[self.key['cost']] = row[self.n_in_row[self.key['cost']]]
            self.col_name_by_bd[self.key['cost_r']] = self.convert_to_R()
            date_time_str = row[self.n_in_row[self.key['term']]]
            date_time_obj = datetime.strptime(date_time_str, '%d.%m.%Y')
            self.col_name_by_bd[self.key['term']] = date_time_obj
            self.sheets_order_dic[self.key['prefix'] + row[self.n_in_row[self.key['order']]]] = sheet_item(
                self.col_name_by_bd)
            self.sheets_order_dic[self.key['prefix'] + row[self.n_in_row[self.key['order']]]].add_to_bd()  # DB

    def scan_google_sheet(self):
        self.scan_reset_isset_sheet_item()
        array_sheet_item = self.get_sheets()
        diff_bool = False

        for name_column in self.col_name:  # сопостовляем параметры заказа с колонкой  в таблице

            i = 0
            for name_column_tab in array_sheet_item[0]:
                if self.col_name[name_column] == name_column_tab:
                    self.n_in_row[name_column] = i
                i += 1

        del array_sheet_item[0]
        for row in array_sheet_item:

            self.col_name_by_bd[self.key['order']] = row[self.n_in_row[self.key['order']]]
            self.col_name_by_bd[self.key['n']] = row[self.n_in_row[self.key['n']]]
            self.col_name_by_bd[self.key['cost']] = row[self.n_in_row[self.key['cost']]]
            self.col_name_by_bd[self.key['cost_r']] = self.convert_to_R()
            date_time_str = row[self.n_in_row[self.key['term']]]
            date_time_obj = datetime.strptime(date_time_str, '%d.%m.%Y')
            self.col_name_by_bd[self.key['term']] = date_time_obj

            if self.sheets_order_dic.get(self.key['prefix'] + row[self.n_in_row[self.key['order']]]):
                self.sheets_order_dic.get(self.key['prefix'] + row[self.n_in_row[self.key['order']]]).isset = True
                bool_result = self.sheets_order_dic.get(
                    self.key['prefix'] + row[self.n_in_row[self.key['order']]]).get_diff(self.col_name_by_bd)
                if not diff_bool:
                    diff_bool = bool_result
            else:
                self.sheets_order_dic[self.key['prefix'] + row[self.n_in_row[self.key['order']]]] = sheet_item(
                    self.col_name_by_bd)
                diff_bool = True

        print(self.scan_isset_sheet_item(), 'for delete')
        del_arr = self.scan_isset_sheet_item()
        if (len(del_arr)):
            diff_bool = True
        self.delete_orders_self(del_arr)
        print('Изменения--', diff_bool)
        return diff_bool

    def get_sheets(self):
        values = client.spreadsheets().values().batchGet(
            spreadsheetId=sheet_id,
            ranges=['Лист1'],
            majorDimension='ROWS').execute()
        result = values.get('valueRanges')
        if result:
            print(result[0].get('values')[0])
        pprint('---')
        return result[0].get('values')

    def test_val(self, key, test='INT'):  # проверка значений int 'INT', float 'FLOAT'
        if self.col_name_by_bd.get(key):
            try:
                if test == 'INT':
                    val = int(self.col_name_by_bd[key])
                    return val
                if test == 'FLOAT':
                    val = float(self.col_name_by_bd[key])
                    return val
                log.ra_info('test = INT or FLOAT')
                return False
            except Exception as e:
                log.ra_warning('---error--class Ra_sheets:' + str(e))
                log.ra_info(
                    'Не верное значение в таблице. Колонка {}: {}'.format(self.col_name[key], self.col_name_by_bd[key]))
                return False
        else:
            log.ra_warning('err---test_val--- не верный ключ!')
            return False

    def convert_to_R(self):
        cost = self.test_val(key='cost', test='FLOAT')
        if cost:

            cosr_r = cost * self.exchange_rate
            self.col_name_by_bd['cost_r'] = cosr_r
            return cosr_r
        else:
            return False

    def for_send(self):
        send_arr = []
        for order in self.sheets_order_dic:
            send_arr.append({
                'n': self.sheets_order_dic[order].n,
                'order': self.sheets_order_dic[order].order,
                'cost': self.sheets_order_dic[order].cost,
                'cost_r': format_to_rub(round(self.sheets_order_dic[order].cost_r, 2)),
                'term': self.sheets_order_dic[order].term.strftime("%d.%m.%Y")
            })
        return send_arr
