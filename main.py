# -*- coding: utf8 -*-
from flask import jsonify
from ra_def_api import *
from  BD_con import *
from ra_my_service import  Ra_log

log = Ra_log('main.py')
log.ra_info('Hi! Start main.py')

my_sheets = Ra_sheets()
my_sheets.start()

# MKDIR
dir = ['ra_cache', 'ra_Log']
try:
    for path in dir:
        if not os.path.isdir(path):
            os.mkdir(path)
            log.ra_info('mkdir:' + path)
        else:
            continue
except OSError as error:
    log.ra_warning(error)


@app.route('/get_api/scan', methods=["GET", "POST"])
def scan():
    data = my_sheets.scan_google_sheet()
    return jsonify([{'diff':data}])

@app.route('/get_api', methods=["GET", "POST"])
def get_google_sheets():
    data = my_sheets.for_send()
    return jsonify(data)


if __name__ == "__main__":
    app.run('0.0.0.0', debug=True)
