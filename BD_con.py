# -*- coding: utf8 -*-
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from dotenv import load_dotenv
from ra_my_service import  *
from flask_cors import CORS
import apiclient
import httplib2
from oauth2client.service_account import ServiceAccountCredentials

log = Ra_log('BD_con')
log.ra_info('Hi! Start BD_con')
app  = Flask(__name__)

CORS(app)
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

KEY_FILE = 'testapi-363710-a8b921e13226.json'
sheet_id = '1Kmb-2Ngc51JQFLfxHdEBdavLT6FGt1ULvlxauUpqQq0'
serviceaccount = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE,['https://www.googleapis.com/auth/spreadsheets','https://www.googleapis.com/auth/drive'])
http_authorize = serviceaccount.authorize(httplib2.Http())
client = apiclient.discovery.build('sheets', 'v4', http = http_authorize)

load_dotenv(dotenv_path)
app.config['CACHE_WAY'] = 'ra_cache'
app.config['DATABASE_URL'] = os.environ.get('DATABASE_URL')
dbURL=app.config['DATABASE_URL']
app.config['MAX_CONTENT_PATH'] = 5000000
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

