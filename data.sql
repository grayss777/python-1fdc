CREATE SCHEMA `test1` ; 
CREATE TABLE `test`.`sheet1` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `n` INT NOT NULL,
  `n_order` VARCHAR(45) NULL,
  `cost` VARCHAR(45) NULL,
  `cost_r` VARCHAR(45) NULL,
  `term` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `n_order_UNIQUE` (`n_order` ASC) VISIBLE);
