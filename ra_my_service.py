# -*- coding: utf8 -*-

import re
import os
import secrets
import logging

class Ra_log(object):
    def __init__(self, parent_file):
        """Constructor
        ra_def_rumida_apps
        """
        self.way_log = 'ra_Log/info_log.log'
        try:
            if not os.path.isdir('ra_Log'):
                os.mkdir('ra_Log')
        except OSError:
            print("Создать директорию %s не удалось" % self.way_log)

        else:
            print("Успешно  %s " % self.way_log)

        self.parent_file = parent_file
        self.logger = logging.getLogger(parent_file)
        self.logger.setLevel(logging.INFO)
        self.fh = logging.FileHandler(self.way_log)
        self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.fh.setFormatter(self.formatter)
        self.logger.addHandler(self.fh)
        self.UI_log =''

    def get_UI_log(self):
        self.UI_log = 'UI_log_' + str(secrets.token_urlsafe(5))
        return self.UI_log


    def ra_info(self,text):
        print('--',self.UI_log,'--',self.parent_file,'-- RA_INFO ', text)
        self.logger.info(self.UI_log + ' ' + text)
        self.UI_log = ''

    def ra_warning(self,text):
        print('--',self.UI_log,'--',self.parent_file,'-- RA_WARNING ', text)
        self.logger.warning(self.UI_log + ' ' + text)
        self.UI_log = ''


def way_to_name(way):# берет последнюю часть пути
    print('way - ',way)
    result = re.split('/', way)
    name = result[-1]
    print('name - ', name)
    print('--------')
    return name
