from BD_con import db, dbURL  # Соединение с BD
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool
from ra_my_service import Ra_log

log = Ra_log('ra_apps.py')
log.ra_info('Hi! Start ra_apps_bd.py')


def ra_sesion_open():
    try:
        eng = db.engine
        eng.dispose()
        some_engine = create_engine(dbURL, poolclass=NullPool,
                                    convert_unicode=True, isolation_level="SERIALIZABLE", pool_pre_ping=True,
                                    echo=False)
        Session = sessionmaker(autocommit=False, autoflush=True, bind=some_engine)
        session = Session()
        return session

    except Exception as e:
        log.ra_warning('err ra_sesion_open' + str(e))
        return False


class Test_sheets(db.Model):  # sheet1
    __tablename__ = 'sheet1'
    id = db.Column(db.Integer(), primary_key=True)
    n = db.Column(db.Integer())
    n_order = db.Column(db.String(45), unique=True)
    cost = db.Column(db.String(45))
    cost_r = db.Column(db.String(45))
    term = db.Column(db.DateTime())

    def add(self):
        session = ra_sesion_open()
        try:
            session.add(self)
            session.flush()
            id = self.id
            session.commit()
            session.close()
            return {'err': False, 'id': str(id)}
        except Exception as e:
            session.rollback()
            session.close()
            log.ra_warning('error_01 add test :' + e.__class__.__name__)
            log.ra_warning('err test' + str(e))
            return {'err': True, 'n_order': str(self.n_order)}

    def update(self, n_order):
        try:
            session = ra_sesion_open()
            query = session.query(Test_sheets).filter_by(n_order=n_order).first()
            try:
                query.cost = self.cost
                query.cost_r = self.cost_r
                query.cost_r = self.Deadline_for_initial_data
                query.term = self.term
                session.flush()

                session.commit()  # Транзакция
                session.close()

                log.ra_info('----- test update ' + self.n_order)
                return True
            except Exception as e:
                session.rollback()
                session.close()
                log.ra_warning('-----ERR test update ' + self.n_order)
                return False


        except Exception as ee:
            session.rollback()
            session.close()
            log.ra_warning('-----ERR Conection server. test update ' + str(ee))
            return False


def delete_order(order_arr):
    session = ra_sesion_open()
    try:
        query = session.query(Test_sheets).filter(Test_sheets.n_order.in_(order_arr)).delete()
        session.flush()
        session.commit()
        session.close()
        return True

    except Exception as e:
        session.rollback()
        session.close()

        log.ra_warning('-----ERROR---delete ' + str(e))
        return False
